<script type="text/javascript">
$(document).ready(function() {$('#ticket').modal({show: false, backdrop: 'static', keyboard: false});});

var id = setTimeout(function(){
 window.location.reload(1);
}, 15000);

function showticket(table){
   $('#printSection').load("<?php echo site_url('pos/showticketKit')?>/"+table);
   clearTimeout(id);
   $('#ticket').modal('show');
}
function PrintTicket() {
   $('.modal-body').removeAttr('id');
   window.print();
   $('.modal-body').attr('id', 'modal-body');
}
function closeModal() {
  window.location.reload(1);
}
</script>
<!-- Page Content -->
<div>
   <div class="container panel p-5">
        <?=!$zones ? '<h4>'.label("NoTables").'</h4>' : '';?>
        <?php foreach ($zones as $zone):?>
        <div class="row">
           <h1 class="choose_store"> <?=$zone->name;?> </h1>
        </div>
        <div class="col-md-12" style="margin-top: 0px; margin-bottom: 15px;">
           <?php foreach ($tables as $table):?>
              <?php if($table->zone_id == $zone->id) {?>
               <a  href="javascript:void(0)" onclick="showticket(<?=$table->id;?>)">
               <div class="col-sm-1 tableListB">
              <?php if($table->time == 'n'){?><span class="tablenotif animated flash infinite">.</span><?php } ?>
              <i class="<?= $table->status == 1 ? 'fas fa-receipt text-success' : 'fas fa-receipt text-brand'; ?>" style="margin-top: 10px; font-size: 30px;"></i><br>
              <h2 style="color: #000; font-size: 16px; margin-top: 0px;"><?=$table->name;?></h2>
           </div> 
         </a>
           <?php } ?>
           <?php endforeach;?>
        </div>
     <?php endforeach;?>
</div>
<!-- /.container -->

<!-- Modal ticket -->
<div class="modal fade" id="ticket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document" id="ticketModal">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="ticket"><?=label("Order");?></h4>
      </div>
      <div class="modal-body" id="modal-body">
         <div id="printSection">
            <!-- Ticket goes here -->
            <center><h1 style="color:#34495E"><?=label("empty");?></h1></center>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default hiddenpr" onclick="closeModal()"><?=label("Close");?></button>
        <button type="button" class="btn btn-add hiddenpr" onclick="PrintTicket()"><?=label("print");?></button>
      </div>
    </div>
 </div>
</div>
<!-- /.Modal -->
