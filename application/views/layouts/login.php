<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>EVAPOS - Restaurant - Cloud Based POS Software</title>
   <link rel="stylesheet" type="text/css"href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">
   <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?=base_url()?>assets/css/login_custom.css" rel="stylesheet">
   <link href="<?=base_url()?>assets/css/animate.css" rel="stylesheet">
   <link href="<?=base_url()?>assets/css/balloon.css" rel="stylesheet">
   <link rel="icon" href="<?=base_url();?>assets/img/ico.png" type="image/x-icon">
</head>
<body id="top">
   <div class="login-4">
      <div class="container animated fadeIn">
         <div class="row">
            <div class="col-lg-12">
               <div class="form-section">
                  <div class="logo-2">
                     <a href="#">
                        <img src="<?=base_url()?>assets/img/evapos_c_png.png" class="animated fadeIn">
                     </a>
                  </div>
                  <h4><?=label('Loginaccount');?></h4>
                  <?php if(isset($message)){echo "<div class='text-danger'>".$message."</div>";}?>
                  <?php  $attributes = array('class' => 'login'); echo form_open('login', $attributes); ?>
                  <div class="form-group form-box">
                     <input type="text" name="username" value="<?=isset($username)?$username:''?>" class="input-text"
                        placeholder="<?=label("Username");?>" required>
                  </div>
                  <div class="form-group form-box">
                     <input type="password" name="password" placeholder="<?=label("Password");?>" class="input-text"
                        required>
                  </div>
                  <div class="form-group mb-0 clearfix">
                     <?php echo form_submit('submit', label("Iniciar Sesion"), "class='btn lbtn-md btn-theme pull-left'"); ?>
                     <?=form_close()?>
                     <button data-balloon-length="large" class="btn lbtn-md pull-right"
                        aria-label="Si usted a perdido los datos de acceso a la plataforma comuniquese con su superior para realizar el cambio de clave."
                        data-balloon-pos="right" disabled>Olvido su Clave?</button>
                  </div>
                  <p><small>© 2020 | EVAPOS - By <a href="https://codenhardware.com" target="_blank">CodenHardware.</a></small></p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/waves.min.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
</body>
</html>