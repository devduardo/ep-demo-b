<!DOCTYPE html>
<html lang="es">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="">
   <meta name="author" content="">
   <title><?=label('title');?> <?= $this->setting->companyname;?></title>
   <script type="text/javascript" src="<?=base_url()?>assets/js/jquery-2.2.4.min.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/loading.js"></script>
   <link rel="stylesheet" href="<?=base_url();?>assets/css/normalize.min.css" type='text/css'>
   <link rel="stylesheet" href="<?=base_url();?>assets/css/reset.min.css" type='text/css'>
   <link rel="stylesheet" href="<?=base_url();?>assets/css/jquery-ui.css" type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
   <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Pinyon+Script" rel="stylesheet">
   <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?=base_url();?>assets/css/bootstrap-horizon.css" rel="stylesheet">
   <link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
   <link rel="stylesheet" href="<?=base_url()?>assets/css/font/fa5/css/all.css">
   <link rel="stylesheet" href="<?=base_url()?>assets/css/font/fa5/css/v4-shims.css">
   <link href="<?=base_url();?>assets/css/summernote.css" rel="stylesheet">
   <link rel="stylesheet" href="<?=base_url()?>assets/css/waves.min.css">
   <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/daterangepicker.css" />
   <link href="<?=base_url()?>assets/css/keyboard-previewkeyset.css" rel="stylesheet">
   <link href="<?=base_url()?>assets/css/keyboard.css" rel="stylesheet">
   <link href="<?=base_url()?>assets/css/select2.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/sweetalert.css">
   <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-datepicker.min.css">
   <link href="<?=base_url()?>assets/css/Style-<?=$this->setting->theme?>.css" rel="stylesheet">
   <link rel="shortcut icon" href="<?=base_url();?>/assets/img/ico.png" type="image/x-icon">
   <link rel="icon" href="<?=base_url();?>/assets/img/ico.png" type="image/x-icon">
   <link href="<?=base_url()?>assets/css/animate.css" rel="stylesheet">
   <link rel="stylesheet" href="<?=base_url()?>assets/css/font/iconmonstr/css/iconmonstr-iconic-font.min.css">
</head>
<body>
   <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
               data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?php if($this->setting->logo){ ?><img src="<?=base_url()?>assets/img/evapos_b_png.png" alt="logo" style='max-height: 45px; max-width: 200px;margin-top:5px;'><?php } else { ?><img src="<?=base_url()?>assets/img/evapos_b_png.png" alt="logo"><?php } ?></a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="flat-box"><a href="<?=base_url();?>"><i
                        class="fas fa-cash-register text-brand"></i> <span
                        class="menu-text"><?=label("POS");?></span></a>
               </li>
               <?php if($this->user->role == "admin"){?>
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle flat-box" data-toggle="dropdown" role="button" aria-haspopup="true"
                     aria-expanded="false"><i class="fas fa-dolly text-brand"></i> <span class="menu-text"><?=label("MyBusiness");?></span><span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li class="flat-box"><a href="<?=base_url()?>sales"><i class="fas fa-angle-right text-brand"></i>
                           <span class="menu-text"><?=label("Sales");?></span></a></li>
                     <li class="flat-box"><a href="<?=base_url()?>stores"><i class="fas fa-angle-right text-brand"></i>
                           <span class="menu-text"><?=label("Stores");?></span></a></li>
                     <li class="flat-box"><a href="<?=base_url()?>products"><i
                              class="fas fa-angle-right text-brand"></i> <span
                              class="menu-text"><?=label("Product");?></span></a></li>
                     <li class="flat-box"><a href="<?=base_url()?>categories"><i
                              class="fas fa-angle-right text-brand"></i> <span
                              class="menu-text"><?=label("ProductList");?></span></a></li>
                     <li class="flat-box"><a href="<?=base_url()?>waiters"><i class="fas fa-angle-right text-brand"></i>
                           <span class="menu-text"><?=label("Waiters");?></span></a></li>
                     <li class="flat-box"><a href="<?=base_url()?>customers"><i
                              class="fas fa-angle-right text-brand"></i> <span
                              class="menu-text"><?=label("Customers");?></span></a></li>
                     <li class="flat-box"><a href="<?=base_url()?>suppliers"><i
                              class="fas fa-angle-right text-brand"></i> <span
                              class="menu-text"><?=label("Suppliers");?></span></a></li>
                     <li class="flat-box"> <a href="<?=base_url()?>expences"><i
                              class="fas fa-angle-right text-brand"></i> <span
                              class="menu-text"><?=label("Expense");?></span></a></li>
                     <li class="flat-box"><a href="<?=base_url()?>categorie_expences"><i
                              class="fas fa-angle-right text-brand"></i> <span
                              class="menu-text"><?=label("ExpenseTypes");?></span></a></li>
                     <li class="flat-box text-brand"><a
                           href="<?=base_url()?>stats"><i class="fas fa-angle-right text-brand"></i> <span
                              class="menu-text"><?=label("Reports");?> (beta)</span></a></li>
                     <li class="flat-box text-brand"><a
                           href="<?=base_url()?>settings?tab=setting"><i class="fas fa-angle-right text-brand"></i>
                           <span class="menu-text"><?=label("Setting");?></span></a></li>
                  </ul>
               </li><?php } ?>
               <li class="flat-box" data-toggle="tooltip" data-placement="bottom" title="<?=label('LiveSupport');?>"><a href="https://www.tidio.com/talk/hqteuxaslzko2uipuwl29lihtdkc3lyw" target="_blank">
               <i class="fas fa-headset text-brand"></i> <span class="menu-text"><?=label("Support");?></span></a>
               </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li><a>
                     <i class="fas fa-user text-brand"></i>
                     <span> <?=label('Hello');?>, <?php echo $this->user->firstname;?>
                     </span>
                  </a>
               </li>
               <li class="flat-box" data-toggle="tooltip" data-placement="bottom" title="<?=label('CurrentTime');?>"><a>
               <i class="far fa-clock text-brand"></i> <span id="clock"></span>
                  </a>
               </li>
               <li class="flat-box" data-toggle="tooltip" data-placement="bottom" title="<?=label('FullScreen');?>">
              <a onclick="toggleFullScreen()"><i class="im im-maximize text-brand"></i><span class="hidden-lg"> <?=label('FullScreen');?></span></a></li>

               <li class="flat-box" data-toggle="tooltip" data-placement="bottom" title="<?=label('Refresh');?>"><a
                     onClick="window.location.reload();"><i class="fas fa-sync text-brand"></i><span class="hidden-lg"> <?=label('Refresh');?></span></a></li>

               <li class="dropdown language">
                  <a href="#" class="dropdown-toggle flat-box" data-toggle="dropdown" role="button" aria-haspopup="true"
                     aria-expanded="false"><i class="fas fa-globe-americas text-brand"></i> <?=label('LnImage');?><span
                        class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li class="flat-box"><a href="<?=base_url()?>dashboard/change/spanish"><img
                              src="<?=base_url()?>assets/img/flags/sp.png" class="flag" alt="language"> Español</a></li>
                     <li class="flat-box"><a href="<?=base_url()?>dashboard/change/english"><img
                              src="<?=base_url()?>assets/img/flags/en.png" class="flag" alt="language"> English</a></li>
                  </ul>
               </li>
               <li class="flat-box"><a href="<?=base_url()?>logout" title="<?=label('LogOut');?>"><i class="fa fa-power-off text-brand"></i> <?=label('LogOut');?></a></li>
            </ul>
         </div>
         <div id="loadingimg"></div>
      </div>
   </nav>
   <!-- Page Content -->
   <?=$yield?>
   <!-- Page Content -->
   <script>
      $('*').contents().each(function () {
         if (this.nodeType === Node.COMMENT_NODE) {
            $(this).remove();
         }
      });
   </script>
   <script>
      function toggleFullScreen() {
         if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
               document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
               document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
               document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
         } else {
            if (document.cancelFullScreen) {
               document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
               document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
               document.webkitCancelFullScreen();
            }
         }
      }
   </script>

   <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.slimscroll.min.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/waves.min.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.keyboard.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.keyboard.extension-all.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.keyboard.extension-extender.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.keyboard.extension-typing.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.mousewheel.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/select2.min.js"></script>
   <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
   <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
   <script src="<?=base_url()?>assets/js/summernote.js"></script>
   <script src="<?=base_url()?>assets/js/Chart.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/moment.min.js"></script>
   <script type="text/javascript" src="<?=base_url()?>assets/js/daterangepicker.js"></script>
   <script src="<?=base_url()?>assets/js/sweetalert.min.js"></script>
   <script src="<?=base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
   <script src="<?=base_url()?>assets/js/jquery.creditCardValidator.js"></script>
   <script src="<?=base_url()?>assets/js/credit-card-scanner.js"></script>
   <script src="<?=base_url()?>assets/js/jquery.redirect.js"></script>
   <script src="<?=base_url()?>assets/js/jquery.form.min.js"></script>
   <script src="<?=base_url()?>assets/js/app.js"></script>
   <script>
      var myVar = setInterval(function() {
         myTimer();
         }, 1000);

      function myTimer() {
         var d = new Date();
         document.getElementById("clock").innerHTML = d.toLocaleTimeString();
         }
   </script> 
</body>

</html>